package com.sukhovilin.com.greatweightclient;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import com.sukhovilin.com.greatweightclient.domain.Person;
import com.sukhovilin.com.greatweightclient.misc.DataProvider;

public class ProfileActivity extends Activity implements ActionMode.Callback, DataProvider<Person, String> {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_activity);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		FragmentTransaction tr = getFragmentManager().beginTransaction();
		ProfileView profileView = new ProfileView();
		tr.replace(R.id.profile_container, profileView);
		tr.commit();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.menu_edit:
			startActionMode(this);
			FragmentTransaction tr = getFragmentManager().beginTransaction();
			ProfileEdit p = new ProfileEdit();
			tr.replace(R.id.profile_container, p);
			tr.commit();
			return true;
		default:
			return false;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.profile, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		mode.setTitle(R.string.txt_profile_edit);
		return true;
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return false;
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {
		init();
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		return false;
	}

	@Override
	public Person provide(String request) {
		Log.i("ACT", "GET PERSON");
		Person person = new Person();
		person.setFullName("Dmitry Sukhovilin");
		person.setEmail("super.dmitry.sukhovilin@yandex.ru");
		person.setBirthday(19790101);
		person.setLanguage("ru");
		person.setSex("m");
		return person;
	}

}
