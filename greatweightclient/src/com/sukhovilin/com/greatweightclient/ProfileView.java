package com.sukhovilin.com.greatweightclient;

import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import com.sukhovilin.com.greatweightclient.domain.Person;
import com.sukhovilin.com.greatweightclient.misc.DataProvider;
import com.sukhovilin.com.greatweightclient.utils.ProfileUtils;

public class ProfileView extends ListFragment {

	private DataProvider<Person, String> provider;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.provider = (DataProvider<Person, String>) activity;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		Activity activity = getActivity();
		String[] from = { "key", "value" };
		int[] to = { R.id.text1, R.id.text2 };
		List<Map<String, String>> data = ProfileUtils.tolist(activity, provider.provide(this.toString()));
		ListAdapter adapter = new SimpleAdapter(activity, data, R.layout.profile_item, from, to);
		setListAdapter(adapter);

	}

}
