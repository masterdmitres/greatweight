package com.sukhovilin.com.greatweightclient;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class WorkoutsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.workouts_activity);

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.workouts, menu);
		return super.onCreateOptionsMenu(menu);
	}

}
