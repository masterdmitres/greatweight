package com.sukhovilin.com.greatweightclient.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Person {

	public static final String FULL_NAME = "full_name";
	public static final String EMAIL = "email";
	public static final String BIRTHDAY = "birthday";
	public static final String SEX = "sex";
	public static final String LANG = "lang";

	@Expose()
	@SerializedName(EMAIL)
	private String email;

	@Expose()
	@SerializedName(FULL_NAME)
	private String fullName;

	@Expose
	@SerializedName(BIRTHDAY)
	private int birthday;

	@Expose
	@SerializedName(SEX)
	private String sex;

	@Expose
	@SerializedName(LANG)
	private String language;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getBirthday() {
		return birthday;
	}

	public void setBirthday(int birthday) {
		this.birthday = birthday;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

}
