package com.sukhovilin.com.greatweightclient.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.sukhovilin.com.greatweightclient.R;
import com.sukhovilin.com.greatweightclient.domain.Person;

public class ProfileUtils {

	public static List<Map<String, String>> tolist(Context context, Person person) {
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		result.add(tomap(context, R.string.profile_full_name, person.getFullName()));
		result.add(tomap(context, R.string.profile_email, person.getEmail()));
		result.add(tomap(context, R.string.profile_birthday, Dates.birthday(person.getBirthday())));
		result.add(tomap(context, R.string.profile_sex, SecUtils.tostring(context, person.getSex())));
		result.add(tomap(context, R.string.profile_language, LangUtils.tostring(context, person.getLanguage())));
		return result;
	}

	//
	private static Map<String, String> tomap(Context context, int res, String value) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("key", context.getString(res));
		map.put("value", value);
		return map;
	}

}
