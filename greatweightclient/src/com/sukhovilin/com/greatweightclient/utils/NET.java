package com.sukhovilin.com.greatweightclient.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.sukhovilin.com.greatweightclient.misc.Predicate;

public class NET {

	public void req(final String url, final Predicate<String> predicate) throws Exception {


		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {

				try{
				//

				int timeoutConnection = 3000;
				int timeoutSocket = 5000;

				//

				HttpParams httpParameters = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
				HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
				HttpClient httpclient = new DefaultHttpClient(httpParameters);

				HttpGet httppost = new HttpGet(url);
				httppost.setHeader("Accept", "application/json");

				List<NameValuePair> nameValuePairs = new LinkedList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("p", "json"));
				//httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				HttpResponse response = httpclient.execute(httppost);
				StatusLine status = response.getStatusLine();

				if (status.getStatusCode() == HttpStatus.SC_OK) {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					response.getEntity().writeTo(out);
					out.close();
					String responseString = out.toString();
					predicate.apply(responseString);
				} else {
					response.getEntity().getContent().close();
					throw new IOException(status.getReasonPhrase());
				}

				}catch(Exception e){
				}
			}
		});
		
		t.start();

	}

}
