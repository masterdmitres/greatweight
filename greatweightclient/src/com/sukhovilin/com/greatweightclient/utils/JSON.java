package com.sukhovilin.com.greatweightclient.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSON {

	private static final Gson GSON;

	static {
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		GSON = builder.create();
	}

	public static <T> T deserialize(Class<T> clazz, String json) {
		return GSON.fromJson(json, clazz);
	}

	public static <T> String serialize(T instance) {
		return GSON.toJson(instance);
	}

}
