//package com.sukhovilin.com.greatweightclient.utils;
//
//import java.io.ByteArrayOutputStream;
//import java.util.LinkedList;
//import java.util.List;
//
//import org.apache.http.HttpResponse;
//import org.apache.http.HttpStatus;
//import org.apache.http.NameValuePair;
//import org.apache.http.StatusLine;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.message.BasicNameValuePair;
//import org.apache.http.params.BasicHttpParams;
//import org.apache.http.params.HttpConnectionParams;
//import org.apache.http.params.HttpParams;
//
//import android.content.Context;
//import android.os.StrictMode;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.rawforce.mobile.androidclient.domain.json.RequestBase;
//import com.rawforce.mobile.androidclient.domain.json.ResponseBase;
//import com.rawforce.mobile.androidclient.services.Action;
//
///**
// * 
// * @author Dmitry Sukhovilin <dmitry.sukhovilin@gmail.com>
// * 
// */
//public class NetworkUtils {
//
//	private final Context context;
//
//	public NetworkUtils(Context context) {
//		this.context = context;
//		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//		StrictMode.setThreadPolicy(policy);
//	}
//
//	public <RESP extends ResponseBase, REQ extends RequestBase> RESP execute(Action action, REQ request, Class<REQ> reqclazz, Class<RESP> respclazz) throws Exception {
//
//		// final String url = "http://192.168.2.100:8080/api/" + action +
//		// ".json";
//		final String url = "http://192.168.0.105:8080/api/" + action.getId() + ".json";
//
//		//
//		int timeoutConnection = 3000;
//		int timeoutSocket = 5000;
//
//		//
//
//		HttpParams httpParameters = new BasicHttpParams();
//		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
//		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
//		HttpClient httpclient = new DefaultHttpClient(httpParameters);
//
//		HttpPost httppost = new HttpPost(url);
//		httppost.setHeader("Accept", "application/json");
//
//		List<NameValuePair> nameValuePairs = new LinkedList<NameValuePair>();
//		String json = JUtils.tojson(request, reqclazz);
//		nameValuePairs.add(new BasicNameValuePair("p", json));
//		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//
//		HttpResponse response = httpclient.execute(httppost);
//
//		StatusLine status = response.getStatusLine();
//		if (status.getStatusCode() == HttpStatus.SC_OK) {
//			ByteArrayOutputStream out = new ByteArrayOutputStream();
//			try {
//				response.getEntity().writeTo(out);
//				String respJson = out.toString();
//				Log.d("1", respJson);
//
//				RESP result = JUtils.fromjson(respJson, respclazz);
//				if (result.error) {
//					String errorCode = "ERROR: " + result.errorCode;
//					Toast.makeText(context, errorCode, Toast.LENGTH_SHORT).show();
//					return null;
//				}
//				//Toast.makeText(context, "OK", Toast.LENGTH_SHORT).show();
//
//				return result;
//
//			} finally {
//				out.close();
//			}
//		}
//
//		// close the connection
//		response.getEntity().getContent().close();
//		throw new Exception(status.getReasonPhrase());
//
//	}
//
//}
