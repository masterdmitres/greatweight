package com.sukhovilin.com.greatweightclient.utils;

import com.sukhovilin.com.greatweightclient.R;

import android.content.Context;

public class LangUtils {

	public static final String tostring(Context context, String lang) {
		if (lang == "en") return context.getString(R.string.profile_lang_english);
		return context.getString(R.string.profile_lang_russian);
	}

}
