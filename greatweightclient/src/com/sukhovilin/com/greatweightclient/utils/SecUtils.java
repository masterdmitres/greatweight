package com.sukhovilin.com.greatweightclient.utils;

import com.sukhovilin.com.greatweightclient.R;

import android.content.Context;

public class SecUtils {

	public static final String tostring(Context context, String sex) {
		if (sex == "m") return context.getString(R.string.profile_sex_male);
		return context.getString(R.string.profile_sex_female);
	}

}
