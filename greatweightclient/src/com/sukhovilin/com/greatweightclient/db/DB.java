package com.sukhovilin.com.greatweightclient.db;

import java.util.List;

import com.sukhovilin.com.greatweightclient.misc.Predicate;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DB extends SQLiteOpenHelper {

	private static final String DB_NAME = "greatweightdb";
	private static final int DB_VERSION = 1;

	public DB(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table profile (id integer primary key autoincrement, name text, email text);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	public <T> void save(T instance) {

	}

	public <T> List<T> find(Class<T> clazz, Predicate<T> predicate) {
		return null;
	}

}
