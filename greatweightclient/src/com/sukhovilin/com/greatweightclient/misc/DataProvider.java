package com.sukhovilin.com.greatweightclient.misc;

public interface DataProvider<T, R> {
	T provide(R request);
}
