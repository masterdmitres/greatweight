package com.sukhovilin.com.greatweightclient.misc;

public interface Predicate<T> {

	boolean apply(T instance) throws Exception;

}
