package com.sukhovilin.com.greatweightclient;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.sukhovilin.com.greatweightclient.domain.Person;
import com.sukhovilin.com.greatweightclient.misc.DataProvider;
import com.sukhovilin.com.greatweightclient.utils.Dates;

public class ProfileEdit extends Fragment {

	private DataProvider<Person, String> proviver;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.proviver = (DataProvider<Person, String>) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {

		Person person = proviver.provide("");
		Activity activity = getActivity();

		View view = inflater.inflate(R.layout.profile_edit, null);

		// full_name

		EditText txtFullName = (EditText) view.findViewById(R.id.txt_full_name);
		txtFullName.setText(person.getFullName());

		// email

		EditText txtEmail = (EditText) view.findViewById(R.id.txt_email);
		txtEmail.setText(person.getEmail());

		// birthday

		EditText txtBirthday = (EditText) view.findViewById(R.id.txt_birthday);
		txtBirthday.setText(Dates.birthday(person.getBirthday()));

		// sex

		Spinner spSex = (Spinner) view.findViewById(R.id.sp_sex);
		String[] sdata = { activity.getString(R.string.profile_sex_male), activity.getString(R.string.profile_sex_female) };
		ArrayAdapter<String> asex = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, sdata);
		spSex.setAdapter(asex);
		
		// lang
		
		Spinner spLang = (Spinner) view.findViewById(R.id.sp_lang);
		String[] ldata = { activity.getString(R.string.profile_lang_english), activity.getString(R.string.profile_lang_russian) };
		ArrayAdapter<String> alang = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, ldata);
		spLang.setAdapter(alang);
		

		return view;
	}

}
