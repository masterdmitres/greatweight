package com.sukhovilin.com.greatweightclient;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class DashboardActivity extends Activity implements View.OnClickListener {

	private Button btnProfile;
	private Button btnWorkouts;
	private Button btnTest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard_activity);

		btnProfile = (Button) findViewById(R.id.btn_dashboard_profile);
		btnProfile.setOnClickListener(this);

		btnWorkouts = (Button) findViewById(R.id.btn_dashboard_workouts);
		btnWorkouts.setOnClickListener(this);

		btnTest = (Button) findViewById(R.id.btn_dashboard_test);
		btnTest.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {

		Intent intent = null;

		switch (v.getId()) {
		case R.id.btn_dashboard_profile:
			intent = new Intent(this, ProfileActivity.class);
			break;
		case R.id.btn_dashboard_workouts:
			intent = new Intent(this, WorkoutsActivity.class);
			break;
		default:
			test();
			break;
		}

		if (intent != null) startActivity(intent);

	}

	private void test() {

		MyTask t = new MyTask();
		t.execute();
	}

	class MyTask extends AsyncTask<Void, Integer, Boolean> {

		@Override
		protected void onPostExecute(Boolean result) {
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			Toast.makeText(DashboardActivity.this, values[0].toString(), Toast.LENGTH_SHORT).show();
		}

		@Override
		protected void onCancelled(Boolean result) {
			super.onCancelled(result);
			Toast.makeText(DashboardActivity.this, result.toString(), Toast.LENGTH_SHORT).show();
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			for (int i = 0; i < 5; i++) {
				if (isCancelled()) return false;
				publishProgress(i);
				if (i == 3) cancel(false);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			return true;
		}

	}

}
