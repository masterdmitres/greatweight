package com.sukhovilin.mygym.processor.util.sync;

public class SyncKey<ID extends Comparable<ID>> {

	private final ID id;
	private long timestamp;
	private boolean update;
	private boolean remove;

	public SyncKey(ID id, long timestamp, boolean update, boolean remove) {
		this.id = id;
		this.timestamp = timestamp;
		this.update = update;
		this.remove = remove;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public boolean isUpdate() {
		return update;
	}

	public void update() {
		this.update = true;
	}

	public boolean isRemove() {
		return remove;
	}

	public void remove() {
		this.remove = true;
	}

	//

	@Override
	public String toString() {
		String str = String.format("{id:%s, ts:%d, up:%s, rm:%s}", id, timestamp, update ? "y" : "n", remove ? "y" : "n");
		return str;
	}

	@Override
	public boolean equals(Object obj) {
		SyncKey<?> other = (SyncKey<?>) obj;
		return id.equals(other.id);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public SyncKey<ID> clone() {
		SyncKey<ID> n = new SyncKey<>(id, timestamp, update, remove);
		return n;
	}

}
