package com.sukhovilin.mygym.processor.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * SignupResponse
 *
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public class SignupResponse extends BaseResponse {

	@Expose
	@SerializedName("token")
	private String token;

	public SignupResponse(String token) {
		this.token = token;
	}

}
