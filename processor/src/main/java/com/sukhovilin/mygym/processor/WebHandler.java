package com.sukhovilin.mygym.processor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

import com.sukhovilin.mygym.processor.util.Timer;

public class WebHandler extends AbstractHandler {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("web handler");
	private Server srv = null;
	private RequestProcessor requestProcessor;

	//

	public void setRequestProcessor(RequestProcessor requestProcessor) {
		this.requestProcessor = requestProcessor;
	}

	//

	public void init() {

		srv = new Server(5007);
		srv.setHandler(this);

		while (true) {

			try {
				srv.start();
				return;
			} catch (Exception e) {
				log.error("fail to start processor, retrying in 10 seconds", e);
			}

			try {
				srv.stop();
			} catch (Exception e1) {
				log.error("fail to stop server", e1);
			}

			Timer.sleep(10000);
		}

	}

	public void destroy() {
		if (srv == null) return;

		try {
			srv.stop();
		} catch (Exception e) {
			log.error("fail to stop server", e);
		}

		try {
			srv.destroy();
		} catch (Exception e) {
			log.error("fail to destroy server", e);
		}
	}

	@Override
	public void handle(String operationId, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String opid = cleanup(operationId);

		log.debug("request, operation: {}", opid);

		String jreq = request.getParameter("json");
		String jresp = requestProcessor.proc(opid, jreq);

		response.setHeader("Server", "req-2.0.0");
		response.setContentType("text/plain; charset=UTF-8");

		if (jresp != null) response.getWriter().write(jresp);

		baseRequest.setHandled(true);
	}

	/*
	 * PRIVATE
	 */

	private String cleanup(String operationId) {
		if (operationId == null || operationId.length() < 2) return null;
		return operationId.substring(1).toLowerCase();
	}

}
