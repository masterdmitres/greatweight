package com.sukhovilin.mygym.processor.util;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

class Req {
	
	@JsonProperty("id")
	public ObjectId id;
	
	@JsonProperty("o")
	public Object object;
}