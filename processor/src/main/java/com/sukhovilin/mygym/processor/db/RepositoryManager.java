package com.sukhovilin.mygym.processor.db;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class  RepositoryManager {

	
	private final Map<String, Repository> cache = new ConcurrentHashMap<>();

	private MongodbDataAccess mongodbDataAccess;
	
	public void setMongodbDataAccess(MongodbDataAccess mongodbDataAccess) {
		this.mongodbDataAccess = mongodbDataAccess;
	}
	
	public <T extends Repository> T lookup(Class<T> clazz) throws Exception {

		if (clazz == null) return null;

		String key = clazz.getSimpleName();

		T repository = (T) cache.get(key);
		if (repository == null) {
			repository = clazz.newInstance();
			repository.setDataAccess(mongodbDataAccess);
			cache.put(key, repository);
		}

		return (T) repository;
	}

}
