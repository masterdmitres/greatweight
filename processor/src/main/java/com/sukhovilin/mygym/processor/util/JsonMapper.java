package com.sukhovilin.mygym.processor.util;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Json Mapper.
 * 
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public class JsonMapper {

	private final Gson gson;

	public JsonMapper() {
		gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	}

	public <T> T deserialize(String json, Class<T> clazz) {
		return gson.fromJson(json, clazz);
	}

	public String serialize(Object o) {
		return gson.toJson(o);
	}

	public static void main(String[] args) {

		ExclusionStrategy strategies = new ExclusionStrategy() {
			@Override
			public boolean shouldSkipField(FieldAttributes f) {
//				System.out.println(f.getName()+" "+f.toString());
				return false;
			}

			@Override
			public boolean shouldSkipClass(Class<?> clazz) {
				System.out.println(clazz);
				return false;
			}
		};

		Gson g = new GsonBuilder().setExclusionStrategies(strategies).excludeFieldsWithoutExposeAnnotation().create();

		Dobject d = new Dobject();
	//	d.id = "myid";
		d.name = "myname";

		String j = g.toJson(d);
		System.out.println(j);

	}
}
