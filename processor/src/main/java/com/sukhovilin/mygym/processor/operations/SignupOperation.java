package com.sukhovilin.mygym.processor.operations;

import org.bson.types.ObjectId;

import com.sukhovilin.mygym.processor.db.SecurityRepository;
import com.sukhovilin.mygym.processor.domain.BaseResponse;
import com.sukhovilin.mygym.processor.domain.SignupRequest;
import com.sukhovilin.mygym.processor.domain.SignupResponse;

public class SignupOperation extends AbstractOperation<SignupRequest> {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("signup operation");
	private SecurityRepository securityRepository;

	public void setSecurityRepository(SecurityRepository securityRepository) {
		this.securityRepository = securityRepository;
	}

	//

	@Override
	public String getOperationId() {
		return "signup";
	}

	@Override
	public Class<SignupRequest> getRequestClass() {
		return SignupRequest.class;
	}

	@Override
	public BaseResponse execute(ObjectId uid, SignupRequest signup) throws Exception {
		log.debug("user: {}", signup.user);
		String token = securityRepository.signup(signup);
		return new SignupResponse(token);
	}
}
