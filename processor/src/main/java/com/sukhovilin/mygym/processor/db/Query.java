package com.sukhovilin.mygym.processor.db;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class Query {

	public static DBObject search(Object value) {
		return search("_id", value);
	}

	public static DBObject search(String field, Object value) {
		return new BasicDBObject(field, value);
	}

}
