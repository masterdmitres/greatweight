package com.sukhovilin.mygym.processor.db;

import org.bson.types.ObjectId;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.sukhovilin.mygym.processor.domain.BaseRequest;
import com.sukhovilin.mygym.processor.domain.Session;
import com.sukhovilin.mygym.processor.domain.SigninRequest;
import com.sukhovilin.mygym.processor.domain.SignupRequest;

/**
 * SecurityRepository
 * 
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public class SecurityRepository extends Repository {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("security repository");

	public String signup(SignupRequest request) throws Exception {
		return null;
	}

	public String signin(SigninRequest signin) {

		DBCollection collection = collection(DBColls.SESSIONS);
		log.debug("===> {}", collection.findOne());

		return null;
	}

	public ObjectId getUserId(BaseRequest request) {

		if (request == null || request.token == null) return null;

		log.debug("looking for user by token: {}", request.token);

		DBCollection cusers = collection(DBColls.SESSIONS);
		DBObject query = Query.search(request.token);
		DBObject doc = cusers.findOne(query);

		return (ObjectId) (doc == null ? null : doc.get(Session.USER_ID));
	}

	public void signout(String token) {

		DBCollection session = collection(DBColls.SESSIONS);
		DBObject q = Query.search(token);
		log.debug("signout, query: {}", q);
		session.remove(q);

	}

}
