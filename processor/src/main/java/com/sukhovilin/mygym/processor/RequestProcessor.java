package com.sukhovilin.mygym.processor;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.sukhovilin.mygym.processor.domain.BaseRequest;
import com.sukhovilin.mygym.processor.domain.BaseResponse;
import com.sukhovilin.mygym.processor.operations.AbstractOperation;
import com.sukhovilin.mygym.processor.operations.OperationManager;
import com.sukhovilin.mygym.processor.util.JsonMapper;

public class RequestProcessor {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("request processor");
	private OperationManager operationManager;
	private JsonMapper jmapper;

	public void setOperationManager(OperationManager operationManager) {
		this.operationManager = operationManager;
	}

	public void setJsonMapper(JsonMapper jmapper) {
		this.jmapper = jmapper;
	}

	public String proc(String opid, String jreq) {

		try {

			AbstractOperation<BaseRequest> operation = operationManager.get(opid);
			if (operation == null) return null;

			BaseRequest request = jmapper.deserialize(jreq, operation.getRequestClass());
			BaseResponse response = operation.execute(new ObjectId(), request);

			return jmapper.serialize(response);

		} catch (Exception e) {
			log.error("fail to execute operation", e);
		}
		return new BasicDBObject("error", 1).toString();
	}

}