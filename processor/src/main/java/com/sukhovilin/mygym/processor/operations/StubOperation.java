package com.sukhovilin.mygym.processor.operations;

import org.bson.types.ObjectId;

import com.sukhovilin.mygym.processor.domain.BaseResponse;
import com.sukhovilin.mygym.processor.domain.SimpleRequest;
import com.sukhovilin.mygym.processor.domain.SimpleResponse;

public class StubOperation extends AbstractOperation<SimpleRequest>{

	@Override
	String getOperationId() {
		return "null";
	}

	@Override
	public Class<SimpleRequest> getRequestClass() {
		return SimpleRequest.class;
	}

	@Override
	public BaseResponse execute(ObjectId uid, SimpleRequest request) throws Exception {
		SimpleResponse resporse = new SimpleResponse();
		resporse.error = 20;
		return resporse;
	}

}
