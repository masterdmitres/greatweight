package com.sukhovilin.mygym.processor.operations;

import org.bson.types.ObjectId;

import com.sukhovilin.mygym.processor.db.SecurityRepository;
import com.sukhovilin.mygym.processor.domain.BaseResponse;
import com.sukhovilin.mygym.processor.domain.SimpleRequest;
import com.sukhovilin.mygym.processor.domain.SimpleResponse;

public class SignoutOperation extends AbstractOperation<SimpleRequest> {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("signout operation");

	//

	@Override
	public String getOperationId() {
		return "signout";
	}

	@Override
	public Class<SimpleRequest> getRequestClass() {
		return SimpleRequest.class;
	}

	@Override
	public BaseResponse execute(ObjectId uid, SimpleRequest request) throws Exception {

		log.debug("signout, token: {}", request.token);

		SecurityRepository securityRepository = lookup(SecurityRepository.class);
		securityRepository.signout(request.token);

		return new SimpleResponse();
	}
}
