package com.sukhovilin.mygym.processor.operations;

import org.bson.types.ObjectId;

import com.sukhovilin.mygym.processor.db.Repository;
import com.sukhovilin.mygym.processor.db.RepositoryManager;
import com.sukhovilin.mygym.processor.domain.BaseRequest;
import com.sukhovilin.mygym.processor.domain.BaseResponse;

public abstract class AbstractOperation<R extends BaseRequest> {

	private RepositoryManager repository;

	public void setRepository(RepositoryManager repository) {
		this.repository = repository;
	}

	protected <T extends Repository> T lookup(Class<T> clazz) throws Exception {
		return repository.lookup(clazz);
	}

	abstract String getOperationId();

	public abstract Class<R> getRequestClass();

	public abstract BaseResponse execute(ObjectId uid, R request) throws Exception;
}
