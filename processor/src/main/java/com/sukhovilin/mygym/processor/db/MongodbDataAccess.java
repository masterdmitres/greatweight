package com.sukhovilin.mygym.processor.db;

import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;

/**
 * MongodbRepository
 * 
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public  class MongodbDataAccess  {

	private final MongoClientOptions opts;
	private MongoClient client;
	private ServerAddress serverAddress;

	//

	public MongodbDataAccess() {
		opts = MongoClientOptions.builder().connectionsPerHost(10).build();
	}

	public void setServerAddress(ServerAddress serverAddress) {
		this.serverAddress = serverAddress;
	}

	public void init() {
		this.client = new MongoClient(serverAddress, opts);
	}

	public void destroy() {
		client.close();
	}

	protected DBCollection collection(String collName) {
		String dbName = "sport";
		return client.getDB(dbName).getCollection(collName);
	}

}
