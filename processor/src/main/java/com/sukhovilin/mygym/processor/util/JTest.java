package com.sukhovilin.mygym.processor.util;

import java.io.IOException;
import java.util.HashMap;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class JTest {

	public static void main(String[] args) throws IOException {

		Dobject obj = new Dobject();
		obj.name = "myname";
		obj.more = "test test";
		
		Req req = new Req();
		req.id = new ObjectId();
		req.object = obj;
		
		
		SimpleModule mo = new SimpleModule("oid");
		mo.addDeserializer(ObjectId.class, new JsonDeserializer<ObjectId>() {
			@Override
			public ObjectId deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
				return new ObjectId(jp.getValueAsString());
			}
		});
		mo.addSerializer(ObjectId.class, new JsonSerializer<ObjectId>() {
			@Override
			public void serialize(ObjectId value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
				jgen.writeString(value.toString());
			}
		});

		ObjectMapper m = new ObjectMapper();
		m.setSerializationInclusion(Include.NON_DEFAULT);
		m.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
		m.registerModule(mo);

		System.out.println(m.writeValueAsString(req));
		String s = m.writeValueAsString(obj);
		System.out.println(s);
		Dobject d1 = m.readValue(s, Dobject.class);

	}

}
