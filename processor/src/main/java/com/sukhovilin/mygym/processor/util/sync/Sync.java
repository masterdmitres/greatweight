package com.sukhovilin.mygym.processor.util.sync;

import java.util.Map;
import java.util.Map.Entry;

public class Sync<V> {

	public void sync(Map<SyncKey<?>, V> a, Map<SyncKey<?>, V> b) {

		fomAtoB(a, b);
		fomBtoA(a, b);
		remove(a, b);
		update(a, b);

	}

	private void update(Map<SyncKey<?>, V> a, Map<SyncKey<?>, V> b) {

		for (Map.Entry<SyncKey<?>, V> ae : a.entrySet()) {
			SyncKey<?> ak = ae.getKey();
			for (Map.Entry<SyncKey<?>, V> be : b.entrySet()) {
				SyncKey<?> bk = be.getKey();
				if (ak.equals(bk)) {
					if (ak.getTimestamp() > bk.getTimestamp()) {
						b.put(bk, ae.getValue());
						bk.setTimestamp(ak.getTimestamp());
						bk.update();
					} else if (bk.getTimestamp() > ak.getTimestamp()) {
						a.put(ak, be.getValue());
						ak.setTimestamp(bk.getTimestamp());
						ak.update();
					} // if
				}
			} // for
		} // for

	}

	private void remove(Map<SyncKey<?>, V> a, Map<SyncKey<?>, V> b) {
		for (Map.Entry<SyncKey<?>, V> ae : a.entrySet()) {
			SyncKey<?> ak = ae.getKey();
			for (Map.Entry<SyncKey<?>, V> be : b.entrySet()) {
				SyncKey<?> bk = be.getKey();
				if (ak.equals(bk)) {
					if (ak.isRemove()) bk.remove();
					if (bk.isRemove()) ak.remove();
				}
			}
		}
	}

	private void fomBtoA(Map<SyncKey<?>, V> a, Map<SyncKey<?>, V> b) {

		for (Map.Entry<SyncKey<?>, V> e : b.entrySet()) {
			SyncKey<?> key = e.getKey();
			if (!a.containsKey(key)) {
				SyncKey<?> nk = (SyncKey<?>) key.clone();
				nk.update();
				a.put(nk, e.getValue());
			}
		}

	}

	private void fomAtoB(Map<SyncKey<?>, V> a, Map<SyncKey<?>, V> b) {

		for (Entry<SyncKey<?>, V> e : a.entrySet()) {
			SyncKey<?> key = e.getKey();
			if (!b.containsKey(key)) {
				SyncKey<?> nk = (SyncKey<?>) key.clone();
				nk.update();
				b.put(nk, e.getValue());
			}
		}

	}

	

}
