package com.sukhovilin.mygym.processor.operations;

import org.bson.types.ObjectId;

import com.sukhovilin.mygym.processor.db.SecurityRepository;
import com.sukhovilin.mygym.processor.domain.AuthResponse;
import com.sukhovilin.mygym.processor.domain.BaseResponse;
import com.sukhovilin.mygym.processor.domain.SigninRequest;

public class SigninOperation extends AbstractOperation<SigninRequest> {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("signin operation");



	//

	@Override
	public String getOperationId() {
		return "signin";
	}

	@Override
	public Class<SigninRequest> getRequestClass() {
		return SigninRequest.class;
	}

	@Override
	public BaseResponse execute(ObjectId uid, SigninRequest request) throws Exception {
		
		SecurityRepository securityRepository = lookup(SecurityRepository.class);
		
		
		String token = securityRepository.signin(request);
		log.debug("operation executed, token: {}", token);

		if (token == null) {
			BaseResponse resp = new AuthResponse(null);
			resp.error = AuthResponse.ERROR;
			return resp;
		}

		return new AuthResponse(token);
	}

}
