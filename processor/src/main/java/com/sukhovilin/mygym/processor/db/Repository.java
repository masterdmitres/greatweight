package com.sukhovilin.mygym.processor.db;

import com.mongodb.DBCollection;

public abstract class Repository {

	private MongodbDataAccess dataAccess;
	
	public void setDataAccess(MongodbDataAccess dataAccess) {
		this.dataAccess = dataAccess;
	}
	
	public DBCollection collection(String collectionName){
		return dataAccess.collection(collectionName);
	}
	
}
