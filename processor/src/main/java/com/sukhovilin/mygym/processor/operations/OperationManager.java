package com.sukhovilin.mygym.processor.operations;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.sukhovilin.mygym.processor.db.RepositoryManager;
import com.sukhovilin.mygym.processor.domain.BaseRequest;

public class OperationManager {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("operation manager");

	private RepositoryManager repositoryManager;
	private Map<String, AbstractOperation<BaseRequest>> cache = new ConcurrentHashMap<>();
	private StubOperation stub = new StubOperation();

	public void setRepositoryManager(RepositoryManager repositoryManager) throws Exception {
		this.repositoryManager = repositoryManager;
		add(SigninOperation.class);
		add(SignoutOperation.class);
		add(SignupOperation.class);
	}

	//

	public AbstractOperation<BaseRequest> get(String operationId) {
		if (operationId == null) return (AbstractOperation) stub;
		AbstractOperation<BaseRequest> abstractOperation = cache.get(operationId);
		if (abstractOperation == null) return (AbstractOperation) stub;
		return abstractOperation;
	}

	/*
	 * PRIVATE
	 */

	private <T extends AbstractOperation<?>> void add(Class<T> clazz) throws Exception {

		log.debug("creating operation: {}", clazz.getSimpleName());
		T operation = clazz.newInstance();

		String opid = operation.getOperationId();
		operation.setRepository(repositoryManager);
		@SuppressWarnings("unchecked")
		AbstractOperation<BaseRequest> op = (AbstractOperation<BaseRequest>) operation;
		cache.put(opid, op);

		log.debug("operation created, opid: {}", opid);
	}
}
