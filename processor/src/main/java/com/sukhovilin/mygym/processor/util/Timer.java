package com.sukhovilin.mygym.processor.util;

public class Timer {

	public static void sleep(long millis) {
		if (millis < 0) return;
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			System.out.println(e);
		}
	}

}
