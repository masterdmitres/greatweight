package com.sukhovilin.mygym.processor.domain;


import com.google.common.base.Objects;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * user of the system.
 * 
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public class User {

	public static final String USER_ID = "_id";
	public static final String EMAIL = "email";
	public static final String FULL_NAME = "full_name";

	//

	@Expose
	@SerializedName(USER_ID)
	public Object userId;

	@Expose
	@SerializedName(EMAIL)
	public String email;

	@Expose
	@SerializedName(FULL_NAME)
	public String fullName;

	//

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add(USER_ID, userId).add(EMAIL, email).add(FULL_NAME, fullName).toString();
	}
}
