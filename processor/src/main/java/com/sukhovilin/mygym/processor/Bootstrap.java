package com.sukhovilin.mygym.processor;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Bootstrap {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Bootstrap.class);

	public static void main(String[] args) {

		AbstractApplicationContext context = new FileSystemXmlApplicationContext("context.xml");

		context.start();
		log.debug("server started!");

		
	}
}
