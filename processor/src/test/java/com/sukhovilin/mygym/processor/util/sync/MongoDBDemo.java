package com.sukhovilin.mygym.processor.util.sync;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class MongoDBDemo {

	public static void main(String[] args) throws Exception {

		while (true) {
			new MongoDBDemo().loop("127.0.0.1", 27017);
		}

	}

	private void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void loop(String host, int port) throws Exception {

		MongoClient c = new MongoClient(host, port);
		try {
			DB db = c.getDB("mydb");
			DBCollection ca = db.getCollection("ca");
			DBCollection cb = db.getCollection("cb");
			dosync(ca, cb);
		} finally {
			c.close();
		}
		sleep(1000);

	}

	private void dosync(DBCollection ca, DBCollection cb) {

		Map<SyncKey<?>, DBObject> map1 = asmap(list(ca));
		Map<SyncKey<?>, DBObject> map2 = asmap(list(cb));

		Sync<DBObject> sync = new Sync<>();
		sync.sync(map1, map2);

		save(map1, ca);
		save(map2, cb);

	}

	private void save(Map<SyncKey<?>, DBObject> map, DBCollection coll) {

		for (Map.Entry<SyncKey<?>, DBObject> e : map.entrySet()) {
			SyncKey<?> key = e.getKey();

			if (key.isUpdate()) {
				coll.save(e.getValue());
				System.out.println("sync update");
			}

			if (key.isRemove()) {
				e.getValue().put("ts", -1L);
				coll.save(e.getValue());
				System.out.println("remove");
			}

		}

	}

	private Map<SyncKey<?>, DBObject> asmap(List<DBObject> docs) {
		Map<SyncKey<?>, DBObject> map = new HashMap<>();
		for (DBObject doc : docs) {
			ObjectId oid = (ObjectId) doc.get("_id");
			Long ts = (Long) doc.get("ts");
			boolean remove = ts == -1;
			SyncKey<ObjectId> key = new SyncKey<ObjectId>(oid, ts, false, remove);
			map.put(key, doc);
		}
		return map;
	}

	private List<DBObject> list(DBCollection coll) {

		List<DBObject> docs = new LinkedList<>();
		DBCursor cur = coll.find();
		while (cur.hasNext()) {
			DBObject doc = cur.next();
			docs.add(doc);
		}
		return docs;
	}

}
