package com.sukhovilin.mygym.processor.util.sync;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class SyncKeyTest {

	@Test
	public void show() {
		long ts = System.currentTimeMillis();
		SyncKey<Integer> key = new SyncKey<Integer>(12, ts, false, false);
		System.out.println(key);
	}

	@Test
	public void synckey_test_equals() {

		SyncKey<Integer> key1 = new SyncKey<Integer>(2, 123, false, false);
		SyncKey<Integer> key2 = new SyncKey<Integer>(2, 1234, false, false);

		Map<SyncKey<?>, String> map = new HashMap<>();
		map.put(key1, "hello");
		String exp = map.get(key2);

		Assert.assertTrue("hello".equals(exp));
	}

}
