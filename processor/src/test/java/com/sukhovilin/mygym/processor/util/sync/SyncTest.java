package com.sukhovilin.mygym.processor.util.sync;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class SyncTest {

	private void show(String label, Map<?, ?> map) {
		System.out.println(String.format("for '%s' =>", label));
		for (Map.Entry<?, ?> e : map.entrySet()) {
			String str = String.format("   @ %s => { %s }", e.getKey(), e.getValue());
			System.out.println(str);
		}
	}

	// @Test
	public void from_a_to_b() {

		Sync<String> sync = new Sync<>();

		Map<SyncKey<?>, String> a = new HashMap<>();
		a.put(new SyncKey<Integer>(10, 123, false, false), "Dmitry");
		a.put(new SyncKey<Integer>(11, 123, false, false), "Denis");

		Map<SyncKey<?>, String> b = new HashMap<>();

		sync.sync(a, b);
		show("a", a);
		show("b", b);
	}

	// @Test
	public void from_b_to_a() {

		Sync<String> sync = new Sync<>();

		Map<SyncKey<?>, String> b = new HashMap<>();
		SyncKey<Integer> key1 = new SyncKey<Integer>(10, 123, false, false);
		b.put(key1, "Dmitry");
		SyncKey<Integer> key2 = new SyncKey<Integer>(11, 123, false, false);
		b.put(key2, "Denis");

		Map<SyncKey<?>, String> a = new HashMap<>();

		sync.sync(a, b);
		show("a", a);
		show("b", b);
	}

	// @Test
	public void remove_test() {

		Sync<String> sync = new Sync<>();

		Map<SyncKey<?>, String> a = new HashMap<>();
		SyncKey<Integer> key1 = new SyncKey<Integer>(10, 1231, false, true);
		a.put(key1, "Dmitry");
		SyncKey<Integer> key2 = new SyncKey<Integer>(11, 1232, false, false);
		a.put(key2, "Denis");
		SyncKey<Integer> key11 = new SyncKey<Integer>(13, 1232, false, false);
		a.put(key11, "Olga");

		Map<SyncKey<?>, String> b = new HashMap<>();
		SyncKey<Integer> key4 = new SyncKey<Integer>(11, 1234, false, true);
		b.put(key4, "Denis");

		SyncKey<Integer> key3 = new SyncKey<Integer>(10, 1233, false, false);
		b.put(key3, "Dmitry");

		sync.sync(a, b);
		show("a", a);
		show("b", b);
	}

	@Test
	public void update_test() {

		Sync<String> sync = new Sync<>();

		Map<SyncKey<?>, String> a = new HashMap<>();
		a.put(new SyncKey<Integer>(10, 123, false, false), "Dmitry A");

		Map<SyncKey<?>, String> b = new HashMap<>();
		b.put(new SyncKey<Integer>(10, 124, false, false), "Dmitry B");

		sync.sync(a, b);
		show("a", a);
		show("b", b);
	}
}
