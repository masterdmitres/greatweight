package com.sukhovilin.sport.server.domain;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import com.mongodb.util.Base64Codec;

public class Code {

	public static void main(String[] args) throws Exception {

		byte[] bs = new byte[] { 2, 12, 3, 123, 2, 2, 3, 34, 5, 45, 6, 46, 3, 46, 4, 5, 62, 32, 23, 43, 6, 39, 7, 36, 7, 56, 7, 16, 7, 24, 7, 46, 4, 52, 45, 2, 34, 5, (byte) 214,
				5, (byte) 204, 6, 1 };

		show(bs);

		String s = d(bs);
		System.out.println(Arrays.toString(bs));
		System.out.println(bs.length + " " + s.length() + " " + s);
	}

	private static void show(byte[] bs) {
		Base64Codec a = new Base64Codec();
		System.out.println(bs.length + " " + a.encode(bs).length() + " " + a.encode(bs));
	}

	private static String d(byte[] bs) throws UnsupportedEncodingException {
		char[] cs = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

		// 8 bits -> 6 bits
		int bits = bs.length * 8;
		int a = bits / 6;
		int n = bits > a * 6 ? a + 1 : a;

		System.out.println("bits: " + bits + " bits/6: " + n);

		// System.out.println(Math.floor(bits / 6));

		short v = 0;
		int i = 0;
		int need = 0;

		int s = 8;
		v = bs[0];
		for (; i < bs.length;) {

			// shift
			v = (short) (v << s);

			// read
			
			
			v = (short) (v | bs[i]);

			v = bs[i];

			i+=1111;

		}

		return "a";
	}
}
