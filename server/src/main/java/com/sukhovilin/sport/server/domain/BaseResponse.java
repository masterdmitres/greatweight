package com.sukhovilin.sport.server.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Base Response
 * 
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public abstract class BaseResponse {

	@Expose
	@SerializedName("error")
	public int error;

	public BaseResponse() {
		this.error = 0;
	}

}
