package com.sukhovilin.sport.server.repository;

import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;

/**
 * MongodbRepository
 * 
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public abstract class MongodbRepository {

	private final MongoClientOptions opts;
	private MongoClient client;
	private ServerAddress serverAddress;

	//

	public MongodbRepository() {
		opts = MongoClientOptions.builder().connectionsPerHost(10).build();
	}

	public void setServerAddress(ServerAddress serverAddress) {
		this.serverAddress = serverAddress;
	}

	public void init() {
		this.client = new MongoClient(serverAddress, opts);
	}

	public void destroy() {
		client.close();
	}

	protected DBCollection collection(String coll_name) {
		String db_name = "sport";
		return client.getDB(db_name).getCollection(coll_name);
	}

}
