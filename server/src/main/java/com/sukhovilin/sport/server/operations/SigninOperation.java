package com.sukhovilin.sport.server.operations;

import org.bson.types.ObjectId;

import com.sukhovilin.sport.server.domain.AuthResponse;
import com.sukhovilin.sport.server.domain.BaseResponse;
import com.sukhovilin.sport.server.domain.SigninRequest;
import com.sukhovilin.sport.server.repository.SecurityRepository;

public class SigninOperation implements Operation<SigninRequest, BaseResponse> {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("signin operation");
	private SecurityRepository securityRepository;

	public void setSecurityRepository(SecurityRepository securityRepository) {
		this.securityRepository = securityRepository;
	}

	//

	@Override
	public String getOperationId() {
		return "signin";
	}

	@Override
	public Class<SigninRequest> getRequestClass() {
		return SigninRequest.class;
	}

	@Override
	public BaseResponse execute(ObjectId uid, SigninRequest request) throws Exception {

		String token = securityRepository.signin(request);
		log.debug("operation executed, token: {}", token);

		if (token == null) {
			BaseResponse resp = new AuthResponse(null);
			resp.error = AuthResponse.ERROR;
			return resp;
		}

		return new AuthResponse(token);
	}

}
