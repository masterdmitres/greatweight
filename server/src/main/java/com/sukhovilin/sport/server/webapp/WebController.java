package com.sukhovilin.sport.server.webapp;

import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sukhovilin.sport.server.domain.BaseRequest;
import com.sukhovilin.sport.server.domain.BaseResponse;
import com.sukhovilin.sport.server.operations.Operation;
import com.sukhovilin.sport.server.repository.SecurityRepository;
import com.sukhovilin.sport.server.utils.JsonMapper;

@Controller
public class WebController {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("controller");
	private JsonMapper jm;
	private OperationHolder operations;
	private SecurityRepository securityRepository;

	//

	public void setJsonMapper(JsonMapper jsonMapper) {
		jm = jsonMapper;
	}

	public void setOperations(OperationHolder operations) {
		this.operations = operations;
	}
	
	public void setSecurityRepository(SecurityRepository securityRepository) {
		this.securityRepository = securityRepository;
	}

	//

	@RequestMapping("/{opid}.json")
	public void handle(@PathVariable("opid") String opid, HttpServletRequest request, HttpServletResponse response) {

		try {
			response.setCharacterEncoding("UTF-8");

			log.debug("processing request, opid: {}", opid);

			String jreq = request.getParameter("json");
			Operation<BaseRequest, BaseResponse> operation = operations.getOperation(opid);

			if (operation == null) {
				log.error("no operation found: {}", opid);
				return;
			}

			Class<BaseRequest> request_class = operation.getRequestClass();
			BaseRequest req = jm.deserialize(jreq, request_class);
			ObjectId uid = securityRepository.getUserId(req);

			log.debug("execution operation, opid: {}, req_class: {}, user_id: {}", opid, request_class.getSimpleName(), uid);

			BaseResponse resp = operation.execute(uid, req);
			String jresp = jm.serialize(resp);

			PrintWriter writer = response.getWriter();
			writer.write(jresp);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
