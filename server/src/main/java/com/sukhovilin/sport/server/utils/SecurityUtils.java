package com.sukhovilin.sport.server.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

public class SecurityUtils {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("security utils");

	private static MessageDigest digest;
	private static SecureRandom random;

	private static final int salt_len = 32;
	private static final int pwd_len = 32;

	static {
		try {
			digest = MessageDigest.getInstance("SHA-256");
			random = new SecureRandom();
		} catch (NoSuchAlgorithmException e) {
			log.error("fail to init security utils", e);
		}
	}

	public static synchronized byte[] encrypt(final byte[] password) {

		// making salt
		byte[] salt = new byte[salt_len];
		random.nextBytes(salt);

		byte[] pwd = new byte[password.length + pwd_len];
		System.arraycopy(salt, 0, pwd, 0, salt_len);
		System.arraycopy(password, 0, pwd, salt_len, password.length);

		// encoding password hash
		digest.reset();
		pwd = digest.digest(pwd);

		// join
		byte[] token = new byte[pwd_len + salt_len];
		System.arraycopy(pwd, 0, token, 0, pwd.length);
		System.arraycopy(salt, 0, token, pwd_len, salt_len);

		return token;
	}

	public static synchronized boolean check(final byte[] token, final byte[] password) {

		// extract salt and password
		byte[] pwd = new byte[pwd_len];
		byte[] salt = new byte[salt_len];
		System.arraycopy(token, 0, pwd, 0, pwd_len);
		System.arraycopy(token, pwd_len, salt, 0, salt_len);

		//
		byte[] hash = new byte[password.length + pwd_len];
		System.arraycopy(salt, 0, hash, 0, salt_len);
		System.arraycopy(password, 0, hash, salt_len, password.length);

		// encoding password hash
		digest.reset();
		hash = digest.digest(hash);

		// checking
		return Arrays.equals(pwd, hash);
	}

	public static String toHexString(byte[] ba) {
		StringBuffer sbuf = new StringBuffer();
		for (byte b : ba) {
			String s = Integer.toHexString((int) (b & 0xff));
			if (s.length() == 1) {
				sbuf.append('0');
			}
			sbuf.append(s);
		}
		return sbuf.toString();
	}

	public static void main(String[] args) throws UnsupportedEncodingException {

		byte[] password = "hellO".getBytes("UTF-8");
		byte[] token = encrypt(password);

		System.out.println(token.length + " " + Arrays.toString(token));

		byte[] password1 = "hellA".getBytes("UTF-8");
		System.out.println(toHexString(token).length() + " " + toHexString(token));
		System.out.println(check(token, password1));
	}

}
