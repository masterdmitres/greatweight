package com.sukhovilin.sport.server.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * AuthResponse
 * 
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public class AuthResponse extends BaseResponse {

	public static final int ERROR = 1;

	@Expose
	@SerializedName("token")
	public String token;

	public AuthResponse(String token) {
		this.token = token;
	}

}
