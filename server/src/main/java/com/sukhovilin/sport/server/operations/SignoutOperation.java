package com.sukhovilin.sport.server.operations;

import org.bson.types.ObjectId;

import com.sukhovilin.sport.server.domain.SimpleRequest;
import com.sukhovilin.sport.server.domain.SimpleResponse;
import com.sukhovilin.sport.server.repository.SecurityRepository;

public class SignoutOperation implements Operation<SimpleRequest, SimpleResponse> {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("signout operation");
	private SecurityRepository securityRepository;

	public void setSecurityRepository(SecurityRepository securityRepository) {
		this.securityRepository = securityRepository;
	}

	//

	@Override
	public String getOperationId() {
		return "signout";
	}

	@Override
	public Class<SimpleRequest> getRequestClass() {
		return SimpleRequest.class;
	}

	@Override
	public SimpleResponse execute(ObjectId uid, SimpleRequest request) throws Exception {
		return new SimpleResponse();
	}

}
