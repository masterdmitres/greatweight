package com.sukhovilin.sport.server.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Json Mapper.
 *
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public class JsonMapper {

	final Gson gson;

	public JsonMapper() {
		gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	}

	public <T> T deserialize(String json, Class<T> clazz) {
		return gson.fromJson(json, clazz);
	}

	public String serialize(Object o) {
		return gson.toJson(o);
	}

}
