package com.sukhovilin.sport.server.operations;

import org.bson.types.ObjectId;

import com.sukhovilin.sport.server.domain.BaseRequest;

public interface Operation<R extends BaseRequest, BaseResponse> {

	String getOperationId();

	Class<R> getRequestClass();

	BaseResponse execute(ObjectId uid, R request) throws Exception;
}
