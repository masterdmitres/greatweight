package com.sukhovilin.sport.server.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * SigninRequest
 *
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public class SigninRequest extends BaseRequest {

	@Expose
	@SerializedName("email")
	public String email;

	@Expose
	@SerializedName("password")
	public String password;

}