package com.sukhovilin.sport.server.repository;

import org.bson.types.ObjectId;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.sukhovilin.sport.server.domain.BaseRequest;
import com.sukhovilin.sport.server.domain.Session;
import com.sukhovilin.sport.server.domain.SigninRequest;
import com.sukhovilin.sport.server.domain.SignupRequest;

/**
 * SecurityRepository
 * 
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public class SecurityRepository extends MongodbRepository {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("security repository");

	public String signup(SignupRequest request) throws Exception {
		return null;
	}

	public String signin(SigninRequest signin) {
		return null;
	}

	public ObjectId getUserId(BaseRequest request) {

		if (request == null || request.token == null) return null;

		log.debug("looking for user by token: {}", request.token);

		DBCollection cusers = collection(DBColls.SESSIONS);
		DBObject query = Query.search(request.token);
		DBObject doc = cusers.findOne(query);

		return (ObjectId) (doc == null ? null : doc.get(Session.USER_ID));
	}

}
