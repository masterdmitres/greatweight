package com.sukhovilin.sport.server.webapp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sukhovilin.sport.server.domain.BaseRequest;
import com.sukhovilin.sport.server.domain.BaseResponse;
import com.sukhovilin.sport.server.operations.Operation;

/**
 * Operation Holder.
 *
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public class OperationHolder {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("operation holder");
	private final Map<String, Operation<? extends BaseRequest, ? extends BaseResponse>> map;

	public OperationHolder() {
		map = new HashMap<>();
	}
	
	public void setOperations(List<Operation<BaseRequest, BaseResponse>> operations){
		for(Operation<BaseRequest, BaseResponse> operation: operations){
			String opid = operation.getOperationId();
			log.debug("adding operation, opid: {}, class: {}", opid, operation.getClass());
			map.put(opid, operation);
		}
	}

	public Operation<BaseRequest, BaseResponse> getOperation(String opid) {
		return (Operation<BaseRequest, BaseResponse>) map.get(opid);
	}

}
