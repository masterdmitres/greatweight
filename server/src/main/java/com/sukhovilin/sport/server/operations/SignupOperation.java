package com.sukhovilin.sport.server.operations;

import org.bson.types.ObjectId;

import com.sukhovilin.sport.server.domain.SignupRequest;
import com.sukhovilin.sport.server.domain.SignupResponse;
import com.sukhovilin.sport.server.repository.SecurityRepository;

public class SignupOperation implements Operation<SignupRequest, SignupResponse> {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("signup operation");
	private SecurityRepository securityRepository;

	public void setSecurityRepository(SecurityRepository securityRepository) {
		this.securityRepository = securityRepository;
	}

	//

	@Override
	public String getOperationId() {
		return "signup";
	}

	@Override
	public Class<SignupRequest> getRequestClass() {
		return SignupRequest.class;
	}

	@Override
	public SignupResponse execute(ObjectId uid, SignupRequest signup) throws Exception {
		log.debug("user: {}", signup.user);
		String token = securityRepository.signup(signup);
		return new SignupResponse(token);
	}
}
