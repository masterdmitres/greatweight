package com.sukhovilin.sport.server.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Base Request
 * 
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public abstract class BaseRequest {

	@Expose
	@SerializedName("token")
	public String token;

}
