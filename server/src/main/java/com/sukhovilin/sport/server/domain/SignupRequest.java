package com.sukhovilin.sport.server.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * SignupRequest
 *
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public class SignupRequest extends BaseRequest {

	@Expose
	@SerializedName("user")
	public User user;

}
