package com.sukhovilin.sport.web.executor.domain;

import org.junit.Assert;
import org.junit.Test;

public class ResponseTest {

	class ResponseInstanceTest extends Response {

	}

	@Test
	public void all_test() {
		ResponseInstanceTest response = new ResponseInstanceTest();

		Assert.assertEquals("ok is default", 1, response.getError());

		// error -> ok=0
		response.setError(true);
		Assert.assertEquals(0, response.getError());

		// normal -> ok=1
		response.setError(false);
		Assert.assertEquals(1, response.getError());
	}

}
