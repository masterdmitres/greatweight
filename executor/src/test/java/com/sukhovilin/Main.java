package com.sukhovilin;

public class Main {
	public static void main(String[] args) {
		Storage storage = new Storage();
		new A(storage);
		new B(storage);
	}
}

class Storage {
	public Storage() {
		System.out.println("storage");
	}
}

class BaseClass {
	public BaseClass(Storage storage) {
		System.out.println("base " + storage);
	}
}

class A extends BaseClass {
	public A(Storage storage) {
		super(storage);
		System.out.println("a");
	}
}

class B extends BaseClass {
	public B(Storage storage) {
		super(storage);
		System.out.println("b");
	}
}
