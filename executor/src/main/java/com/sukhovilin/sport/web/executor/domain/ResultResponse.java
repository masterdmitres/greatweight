package com.sukhovilin.sport.web.executor.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResultResponse extends Response {

	@JsonProperty(value = "result")
	private final String result;

	public ResultResponse(String result) {
		this.result = result;
	}

	public String getResult() {
		return result;
	}

}
