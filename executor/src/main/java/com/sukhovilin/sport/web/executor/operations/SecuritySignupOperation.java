package com.sukhovilin.sport.web.executor.operations;

import org.bson.types.ObjectId;

import com.sukhovilin.sport.web.executor.database.RepositoryFactory;
import com.sukhovilin.sport.web.executor.database.SecurityRepository;
import com.sukhovilin.sport.web.executor.domain.Request;
import com.sukhovilin.sport.web.executor.domain.Response;
import com.sukhovilin.sport.web.executor.domain.ResultResponse;
import com.sukhovilin.sport.web.executor.domain.SignupRequest;
import com.sukhovilin.sport.web.executor.misc.Config;

public class SecuritySignupOperation extends Operation<SignupRequest> {

	public SecuritySignupOperation(RepositoryFactory factory) {
		super(factory);
	}

	@Override
	public boolean allow(ObjectId pid) {
		return true;
	}

	@Override
	public Class<? extends Request> getRequestClass() {
		return SignupRequest.class;
	}

	@Override
	public Response execute(ObjectId pid, Config config, SignupRequest request) throws Exception {

		SecurityRepository repository = lookup(SecurityRepository.class);
		ObjectId newpid = repository.signup(request);
		return new ResultResponse(newpid.toString());
	}

}
