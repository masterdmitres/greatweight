package com.sukhovilin.sport.web.executor.database;

import java.net.UnknownHostException;

import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.sukhovilin.sport.web.executor.misc.Config;

public class MongoConnector {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("mongo connector");
	private MongoClient client;
	private String dbName;
	private Config config;

	public MongoConnector(Config config) throws UnknownHostException {
		this.config = config;
		//
		this.dbName = config.get("mongodb.dbname");
	}

	public DBCollection getCollection(String collectionName) throws Exception {
		if (client == null) throw new Exception("no connection to db");
		return client.getDB(dbName).getCollection(collectionName);
	}

	public void connect() throws Exception {
		String host = config.get("mongodb.host");
		int port = Integer.valueOf(config.get("mongodb.port"));

		ServerAddress address = new ServerAddress(host, port);
		MongoClientOptions options = new MongoClientOptions.Builder().autoConnectRetry(true).build();

		log.info("opening mongodb connection {}:{} ...", host, port);
		client = new MongoClient(address, options);
	}

	public void disconnect() throws Exception {
		if (client == null) return;
		log.info("closing mongodb connection...");
		client.close();
		client = null;
	}
}
