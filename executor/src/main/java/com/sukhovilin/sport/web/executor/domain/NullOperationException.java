package com.sukhovilin.sport.web.executor.domain;

public class NullOperationException extends ExecutorException {

	private static final long serialVersionUID = 3977013017902496544L;

	public NullOperationException() {
		super("no operation");
	}
}
