package com.sukhovilin.sport.web.executor.database;

import java.util.Date;

import com.mongodb.DBObject;
import com.sukhovilin.sport.web.executor.misc.Config;

public abstract class Repository {

	public static final String COLL_PERSONS="persons";
	
	protected final Config config;
	protected final MongoConnector connector;

	public Repository(Config config, MongoConnector connector) {
		this.config = config;
		this.connector = connector;
	}

	protected void fillValues(DBObject doc){
		doc.put("cdate", new Date());
	}
}
