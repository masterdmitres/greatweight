package com.sukhovilin.sport.web.executor.operations;

import com.sukhovilin.sport.web.executor.domain.ExecutorException;

public class SecurityException extends ExecutorException{

	private static final long serialVersionUID = -8974639200356026232L;
	
	public SecurityException(String reason) {
		super(reason);
	}



}
