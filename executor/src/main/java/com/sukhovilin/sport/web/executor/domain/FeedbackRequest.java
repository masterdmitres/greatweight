package com.sukhovilin.sport.web.executor.domain;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FeedbackRequest extends Request {

	@JsonProperty("pid")
	private ObjectId personId;

	@JsonProperty(value = "email")
	private String email;

	@JsonProperty(value = "text")
	private String text;

	public ObjectId getPersonId() {
		return personId;
	}

	public void setPersonId(ObjectId personId) {
		this.personId = personId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
