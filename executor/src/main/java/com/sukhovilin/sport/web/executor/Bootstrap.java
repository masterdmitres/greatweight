package com.sukhovilin.sport.web.executor;

import org.bson.types.ObjectId;

import com.sukhovilin.sport.web.executor.database.MongoConnector;
import com.sukhovilin.sport.web.executor.database.RepositoryFactory;
import com.sukhovilin.sport.web.executor.domain.FeedbackRequest;
import com.sukhovilin.sport.web.executor.domain.SignupRequest;
import com.sukhovilin.sport.web.executor.misc.Config;
import com.sukhovilin.sport.web.executor.utils.JsonMapper;

public class Bootstrap {

	public static void main(String[] args) throws Exception {

		Config config = new Config();
		MongoConnector connector = new MongoConnector(config);
		connector.connect();
		RepositoryFactory factory = new RepositoryFactory(config, connector);
		Executor executor = new Executor(config, factory);

		
		
		SignupRequest signup = new SignupRequest();
		signup.setEmail("dmitry.sukhovilin@yandex.ru");
		signup.setFullName("Dmitry Sukhovilin");
		signup.setBirthday(19790101);
		signup.setSex("m");
		signup.setPassword("12e");
		
		FeedbackRequest feedback = new FeedbackRequest();
		feedback.setEmail("myemail@email.com");
		feedback.setText("hello world");

		executor.test("feedback-add", new ObjectId(), feedback);

		connector.disconnect();
	}

}
