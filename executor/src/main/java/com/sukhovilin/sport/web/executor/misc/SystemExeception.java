package com.sukhovilin.sport.web.executor.misc;

import com.sukhovilin.sport.web.executor.domain.ExecutorException;

public class SystemExeception extends ExecutorException {

	private static final long serialVersionUID = -1456155247846456550L;

	public SystemExeception() {
		super("system");
	}

}
