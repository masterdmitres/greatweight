package com.sukhovilin.sport.web.executor.operations;

import java.util.HashMap;
import java.util.Map;

import com.sukhovilin.sport.web.executor.database.RepositoryFactory;
import com.sukhovilin.sport.web.executor.domain.Request;

public class OperationHolder {

	private final Map<String, Operation<? extends Request>> map = new HashMap<String, Operation<? extends Request>>();
	private final Operation<?> nullop;

	public OperationHolder(RepositoryFactory factory) {
		nullop = new NullOperation(factory);
		map.put("feedback-add", new FeedbackAddOperation(factory));
		map.put("security-signup", new SecuritySignupOperation(factory));
		map.put("security-signin", new SecuritySigninOperation(factory));
	}

	@SuppressWarnings("unchecked")
	public Operation<Request> get(String action) {
		Operation<Request> operation = (Operation<Request>) map.get(action);
		if (operation == null) return (Operation<Request>) nullop;
		return operation;
	}

}
