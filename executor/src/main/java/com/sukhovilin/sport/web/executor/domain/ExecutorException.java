package com.sukhovilin.sport.web.executor.domain;

import java.util.HashMap;
import java.util.Map;

import com.mongodb.util.JSON;

public abstract class ExecutorException extends Exception {

	private static final long serialVersionUID = 7457017439437735199L;
	private final Map<String, Object> values = new HashMap<>();

	public ExecutorException(String reason) {
		add("ok", 0);
		add("type", "security");
		add("reason", reason);
	}

	protected void add(String key, Object value) {
		values.put(key, value);
	}

	public String json() {
		final StringBuilder sb = new StringBuilder();
		JSON.serialize(values, sb);
		return sb.toString();
	}
}
