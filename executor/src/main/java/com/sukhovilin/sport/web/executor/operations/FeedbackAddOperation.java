package com.sukhovilin.sport.web.executor.operations;

import org.bson.types.ObjectId;

import com.sukhovilin.sport.web.executor.database.FeedbackRepository;
import com.sukhovilin.sport.web.executor.database.RepositoryFactory;
import com.sukhovilin.sport.web.executor.domain.FeedbackRequest;
import com.sukhovilin.sport.web.executor.domain.Request;
import com.sukhovilin.sport.web.executor.domain.Response;
import com.sukhovilin.sport.web.executor.domain.ResultResponse;
import com.sukhovilin.sport.web.executor.misc.Config;

public class FeedbackAddOperation extends Operation<FeedbackRequest> {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(FeedbackAddOperation.class);

	public FeedbackAddOperation(RepositoryFactory factory) {
		super(factory);
	}

	@Override
	public boolean allow(ObjectId pid) {
		return true;
	}

	@Override
	public Class<? extends Request> getRequestClass() {
		return FeedbackRequest.class;
	}

	@Override
	public Response execute(ObjectId pid, Config config, FeedbackRequest request) throws Exception {

		FeedbackRepository repository = lookup(FeedbackRepository.class);
		request.setPersonId(pid);
		ObjectId id = repository.add(request);
		log.debug("feedback saved: {}", id);
		ResultResponse response = new ResultResponse("ok");
		return response;
	}

}
