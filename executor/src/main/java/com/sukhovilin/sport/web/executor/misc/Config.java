package com.sukhovilin.sport.web.executor.misc;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

	private final Properties props = new Properties();

	public Config() throws IOException {
		try (InputStream in = new FileInputStream("config.properties")) {
			props.load(in);
		}
	}

	public String get(String key) {
		return props.getProperty(key);
	}

}
