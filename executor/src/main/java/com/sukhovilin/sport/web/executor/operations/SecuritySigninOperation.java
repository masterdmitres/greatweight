package com.sukhovilin.sport.web.executor.operations;

import org.bson.types.ObjectId;

import com.mongodb.DBCollection;
import com.sukhovilin.sport.web.executor.database.RepositoryFactory;
import com.sukhovilin.sport.web.executor.database.SecurityRepository;
import com.sukhovilin.sport.web.executor.domain.Request;
import com.sukhovilin.sport.web.executor.domain.Response;
import com.sukhovilin.sport.web.executor.domain.ResultResponse;
import com.sukhovilin.sport.web.executor.domain.SigninRequest;
import com.sukhovilin.sport.web.executor.misc.Config;

public class SecuritySigninOperation extends Operation<SigninRequest> {

	public SecuritySigninOperation(RepositoryFactory factory) {
		super(factory);
	}

	@Override
	public boolean allow(ObjectId pid) {
		return true;
	}

	@Override
	public Class<? extends Request> getRequestClass() {
		return SigninRequest.class;
	}

	@Override
	public Response execute(ObjectId pid, Config config, SigninRequest request) throws Exception {
		SecurityRepository repository = lookup(SecurityRepository.class);
		repository.signin(request);
		ResultResponse r = new ResultResponse("");
		return null;
	}

}
