package com.sukhovilin.sport.web.executor.database;

import org.bson.types.ObjectId;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.sukhovilin.sport.web.executor.domain.FeedbackRequest;
import com.sukhovilin.sport.web.executor.misc.Config;
import com.sukhovilin.sport.web.executor.misc.Consts;
import com.sukhovilin.sport.web.executor.utils.JsonMapper;

public class FeedbackRepository extends Repository {

	public FeedbackRepository(Config config, MongoConnector connector) {
		super(config, connector);
	}

	public ObjectId add(FeedbackRequest request) throws Exception {
		DBCollection coll = connector.getCollection(Consts.Collections.FEEDBACKS);
		DBObject doc = JsonMapper.convert(request);
		fillValues(doc);
		coll.insert(doc);
		return (ObjectId) doc.get("_id");
	}

}
