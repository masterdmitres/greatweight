package com.sukhovilin.sport.web.executor.database;

import java.util.HashMap;
import java.util.Map;

import com.sukhovilin.sport.web.executor.misc.Config;

public class RepositoryFactory {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("repository helper");
	private static final Map<String, Repository> map = new HashMap<>();
	private final Config config;
	private final MongoConnector connector;

	public RepositoryFactory(Config config, MongoConnector connector) {
		this.config = config;
		this.connector = connector;
	}

	@SuppressWarnings("unchecked")
	public <T extends Repository> T get(Class<T> clazz) throws Exception {

		String name = clazz.getSimpleName();
		Repository repository = map.get(name);
		if (repository == null) {
			log.debug("creating repository {} ...", name);
			repository = clazz.getConstructor(Config.class, MongoConnector.class).newInstance(config, connector);
			map.put(name, repository);
		}

		return (T) repository;
	}
}
