package com.sukhovilin.sport.web.executor.database.misc;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class Query {

	public static DBObject searchActiveById(Object id) {
		return new BasicDBObject("active", true).append("_id", id);
	}

	public static DBObject fields(String... fields) {
		DBObject q = new BasicDBObject();
		for (String field : fields) {
			q.put(field, 1);
		}
		return q;
	}

}
