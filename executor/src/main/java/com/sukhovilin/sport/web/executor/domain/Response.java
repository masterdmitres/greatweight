package com.sukhovilin.sport.web.executor.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class Response {

	@JsonProperty("ok")
	private int error = 1;

	public int getError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error ? 0 : 1;
	}

}
