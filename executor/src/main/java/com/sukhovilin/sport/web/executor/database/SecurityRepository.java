package com.sukhovilin.sport.web.executor.database;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.sukhovilin.sport.web.executor.database.misc.Query;
import com.sukhovilin.sport.web.executor.domain.SigninRequest;
import com.sukhovilin.sport.web.executor.domain.SignupRequest;
import com.sukhovilin.sport.web.executor.misc.Config;
import com.sukhovilin.sport.web.executor.misc.Consts;
import com.sukhovilin.sport.web.executor.operations.SecurityException;

public class SecurityRepository extends Repository {

	public SecurityRepository(Config config, MongoConnector connector) {
		super(config, connector);
	}

	public ObjectId pid(String token) throws Exception {
		DBCollection coll = connector.getCollection(COLL_PERSONS);
		DBObject query = new BasicDBObject("token", token).append("active", true);
		DBObject fields = Query.fields();
		DBObject doc = coll.findOne(query, fields);
		if (doc == null) return null;
		return (ObjectId) doc.get("_id");
	}

	public ObjectId signup(SignupRequest request) throws Exception {

		DBCollection coll = connector.getCollection(COLL_PERSONS);

		DBObject q = new BasicDBObject("email", request.getEmail()).append("active", true);

		if (coll.findOne(q) != null) {
			throw new SecurityException(Consts.Errors.EMAIL_EXISTS);
		}

		return new ObjectId();
	}

	public void signin(SigninRequest request) throws Exception {
		DBCollection coll = connector.getCollection(COLL_PERSONS);

	}
}
