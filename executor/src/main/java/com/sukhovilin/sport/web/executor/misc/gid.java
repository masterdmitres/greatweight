package com.sukhovilin.sport.web.executor.misc;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CountDownLatch;

public class gid {

	// 2014-01-01 00:00:00 UTC
	private static final long offset = 1388534400000L;
	private static long last = 0L;
	private static int inc = 0;

	public synchronized static long nextval() {

		long time = (System.currentTimeMillis() - offset);
		time <<= 16;

		if (last == time) {
			inc++;
			time |= inc;
		} else {
			inc = 0;
			last = time;
		}
		return time;
	}

	public static void main(String[] args) throws InterruptedException {

		final Set<Long> s = new ConcurrentSkipListSet<>();
		final CountDownLatch l = new CountDownLatch(50);

		for (int t = 0; t < 50; t++) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					for (int i = 0; i < 200_000; i++) {
						long id = nextval();
						s.add(id);
					}
					l.countDown();
				}
			}).start();
		}

		l.await();
		System.out.printf("%,d", s.size());

	}

}
