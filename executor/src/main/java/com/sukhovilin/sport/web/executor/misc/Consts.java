package com.sukhovilin.sport.web.executor.misc;

public class Consts {

	public static class Errors {
		public static final String EMAIL_EXISTS = "email_exists";
	}

	public static class Collections {
		public static final String FEEDBACKS = "feedbacks";
	}

}
