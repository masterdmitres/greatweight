package com.sukhovilin.sport.web.executor;

import org.bson.types.ObjectId;

import com.sukhovilin.sport.web.executor.database.RepositoryFactory;
import com.sukhovilin.sport.web.executor.database.SecurityRepository;
import com.sukhovilin.sport.web.executor.domain.ExecutorException;
import com.sukhovilin.sport.web.executor.domain.Request;
import com.sukhovilin.sport.web.executor.domain.Response;
import com.sukhovilin.sport.web.executor.misc.Config;
import com.sukhovilin.sport.web.executor.misc.SystemExeception;
import com.sukhovilin.sport.web.executor.operations.Operation;
import com.sukhovilin.sport.web.executor.operations.OperationHolder;
import com.sukhovilin.sport.web.executor.operations.SecurityException;
import com.sukhovilin.sport.web.executor.utils.JsonMapper;

public class Executor {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("executor");
	private final Config config;
	private final OperationHolder operations;
	private final RepositoryFactory factory;
	private final ExecutorException sysex = new SystemExeception();

	public Executor(Config config, RepositoryFactory factory) {
		this.config = config;
		this.factory = factory;
		this.operations = new OperationHolder(factory);
	}

	public String execute(String token, String action, String data) throws Exception {

		try {
			SecurityRepository security = factory.get(SecurityRepository.class);

			Operation<Request> operation = operations.get(action);
			ObjectId pid = security.pid(token);
			if (!operation.allow(pid)) throw new SecurityException("wrong token");
			Class<?> clazz = operation.getRequestClass();
			log.debug("prepare action: {}, operation: {}, pid: {}", action, operation.getClass().getSimpleName(), pid);
			Request request = (Request) JsonMapper.deserialize(data, clazz);
			Response response = operation.execute(pid, config, request);
			String json = JsonMapper.serialize(response);
			return json;

		} catch (ExecutorException e) {
			return e.json();
		} catch (Exception e) {
			log.error("fatal fail", e);
			return sysex.json();
		}

	}

	public Response test(String action, ObjectId pid, Request request) throws Exception {
		Operation<Request> operation = operations.get(action);
		log.debug("test operation: {} ", operation);
		return operation.execute(pid, config, request);
	}

}
