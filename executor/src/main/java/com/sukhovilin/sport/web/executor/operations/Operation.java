package com.sukhovilin.sport.web.executor.operations;

import org.bson.types.ObjectId;

import com.sukhovilin.sport.web.executor.database.Repository;
import com.sukhovilin.sport.web.executor.database.RepositoryFactory;
import com.sukhovilin.sport.web.executor.domain.ExecutorException;
import com.sukhovilin.sport.web.executor.domain.Request;
import com.sukhovilin.sport.web.executor.domain.Response;
import com.sukhovilin.sport.web.executor.misc.Config;
import com.sukhovilin.sport.web.executor.misc.SystemExeception;

public abstract class Operation<R extends Request> {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("operation");
	private RepositoryFactory factory;

	public Operation(RepositoryFactory factory) {
		this.factory = factory;
	}

	public abstract Response execute(ObjectId pid, Config config, R request) throws Exception;

	public abstract Class<? extends Request> getRequestClass();

	public abstract boolean allow(ObjectId pid);

	protected <T extends Repository> T lookup(Class<T> clazz) throws ExecutorException {
		try {
			return factory.get(clazz);
		} catch (Exception e) {
			log.debug("", e);
			throw new SystemExeception();
		}
	}

	@Override
	public String toString() {
		return "OP::" + getClass().getSimpleName();
	}

}
