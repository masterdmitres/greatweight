package com.sukhovilin.sport.web.executor.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Dates {

	private static final DateFormat text_full =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
	private static final DateFormat text_short =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final DateFormat sys_full =new SimpleDateFormat("yyyyMMddHHmmssSSS");
	private static final DateFormat sys_short =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	
	public static void main(String[] args) {
		
		
		String s= text_short.format(new Date());
		System.out.println("date: <"+s+">");
		
	}
	
}
