package com.sukhovilin.sport.web.executor.operations;

import org.bson.types.ObjectId;

import com.sukhovilin.sport.web.executor.database.RepositoryFactory;
import com.sukhovilin.sport.web.executor.domain.ExecutorException;
import com.sukhovilin.sport.web.executor.domain.NullOperationException;
import com.sukhovilin.sport.web.executor.domain.NullRequest;
import com.sukhovilin.sport.web.executor.domain.Request;
import com.sukhovilin.sport.web.executor.domain.Response;
import com.sukhovilin.sport.web.executor.misc.Config;

public class NullOperation extends Operation<NullRequest> {

	public NullOperation(RepositoryFactory factory) {
		super(factory);
	}

	@Override
	public boolean allow(ObjectId pid) {
		return true;
	}

	@Override
	public Class<? extends Request> getRequestClass() {
		return NullRequest.class;
	}

	@Override
	public Response execute(ObjectId pid, Config config, NullRequest request) throws ExecutorException {
		throw new NullOperationException();
	}

}
