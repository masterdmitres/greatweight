package com.sukhovilin.sport.web.executor.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

import org.postgresql.Driver;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;
import com.jolbox.bonecp.BoneCPDataSource;

public class Run {

	public static void main(String[] args) throws SQLException, InterruptedException {

//		Driver d = new Driver();
//		String url = "jdbc:postgresql://127.0.0.1:5432/mydb";
//		Properties info = new Properties();
//		info.put("user", "dm");
//		info.put("password", "qwerty");

		BoneCPConfig config = new BoneCPConfig();
		config.setUsername("dm");
		config.setPassword("qwerty");
		config.setJdbcUrl("jdbc:postgresql://127.0.0.1:5432/mydb");

		//		bonecp.driverClass = com.mysql.jdbc.Driver
//		bonecp.jdbcUrl = jdbc:mysql://tardis/smsmail
//		bonecp.username = dm
//		bonecp.password = qwerty_00
//		bonecp.idleConnectionTestPeriodInMinutes = 3
//		bonecp.idleMaxAgeInMinutes = 240
//		bonecp.maxConnectionsPerPartition = 3
//		bonecp.minConnectionsPerPartition = 1
//		bonecp.partitionCount = 2
//		bonecp.acquireIncrement = 1
//		bonecp.statementsCacheSize = 10
		
		BoneCPDataSource a = new BoneCPDataSource(config );
		
		
		
		for (;;) {
			try {
				try (Connection cnn = a.getConnection()) {
					try (PreparedStatement st = cnn.prepareStatement("SELECT p.pid, n.note_id, p.full_name, n.note FROM persons p JOIN notes n ON p.pid = n.pid;")) {
						try (ResultSet rs = st.executeQuery()) {

							while (rs.next()) {
								String s = "";
								for (int c = 1; c <= 4; c++) {
									s += rs.getObject(c) + " ";
								}
								System.out.println(s);
							}
						}
					}
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} // try

			System.out.println(new Date());
			System.out.println();
			Thread.sleep(2000);
		}
	}// for

	// try (PreparedStatement st =
	// cnn.prepareStatement("INSERT INTO persons (pid, full_name) VALUES (?,?);"))
	// {
	// st.setLong(1, pid);
	// st.setString(2, "Dmitry Sukhovilin");
	// st.execute();
	// }
	//
	// try (PreparedStatement st =
	// cnn.prepareStatement("INSERT INTO notes (note_id, pid, note) VALUES (?,?,?);"))
	// {
	// st.setLong(1, noteId);
	// st.setLong(2, pid);
	// st.setString(3, "Это комментарий");
	// st.execute();
	// }

	// cnn.commit();

}
