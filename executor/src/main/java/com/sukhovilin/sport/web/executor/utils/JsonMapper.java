package com.sukhovilin.sport.web.executor.utils;

import java.io.IOException;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Json Mapper.
 *
 * @author Dmitry Sukhovilin <dmitry.sukhovilin@yandex.ru>
 */
public class JsonMapper {

	private static final ObjectMapper mapper = new ObjectMapper();

	static {

		final SimpleModule oidModule = new SimpleModule("$id", Version.unknownVersion());

		oidModule.addSerializer(ObjectId.class, new JsonSerializer<ObjectId>() {
			@Override
			public void serialize(ObjectId value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
				jgen.writeObject(value);
			}
		});

		oidModule.addDeserializer(ObjectId.class, new JsonDeserializer<ObjectId>() {
			@Override
			public ObjectId deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
				JsonNode jid = ((JsonNode) jp.readValueAsTree());
				String oid = jid.asText();
				return new ObjectId(oid);
			}
		});

		mapper.registerModule(oidModule);
		mapper.setSerializationInclusion(Include.NON_NULL); // проверим как работает NON_DEFAULT вместо NOT_NULL
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

	}

	private JsonMapper() {
	}

	// methods

	public static <T> DBObject convert(T instance) throws IOException {
		if (instance == null) return null;
		return mapper.convertValue(instance, BasicDBObject.class);
	}

	public static <T> T convert(DBObject document, Class<T> clazz) throws IOException {
		if (document == null || clazz == null) return null;
		return mapper.convertValue(document, clazz);
	}

	public static String serialize(Object object) throws IOException {
		if (object == null) return null;
		return mapper.writeValueAsString(object);
	}

	public static <T> T deserialize(String json, Class<T> clazz) throws IOException {
		if (json == null || clazz == null) return null;
		return mapper.readValue(json, clazz);
	}

}
