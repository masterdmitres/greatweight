package com.sukhovilin.db.pgmail.etc;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("configuration");
	public final Properties props = new Properties();

	public void load(String fileName) throws IOException {
		try (InputStream in = new FileInputStream(fileName)) {
			props.clear();
			log.debug("loading configuration");
			props.load(in);
			log.debug("configuration loaded: {}", props.size());
		}
	}

	public String get(String key) {
		return props.getProperty(key);
	}

}
