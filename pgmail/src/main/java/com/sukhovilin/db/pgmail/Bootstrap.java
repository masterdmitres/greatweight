package com.sukhovilin.db.pgmail;

import java.io.IOException;
import java.sql.SQLException;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.sukhovilin.db.pgmail.etc.Config;
import com.sukhovilin.db.pgmail.etc.Query;
import com.sukhovilin.db.pgmail.ioc.IoCModule;

/**
 * Hello world!
 * 
 */
public class Bootstrap {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("bootstrap");

	public static void main(String[] args) throws InterruptedException, IOException, SQLException {

		Injector injector = Guice.createInjector(new IoCModule());

		Query query = injector.getInstance(Query.class);
		query.load("queries.sql");

		Config config = injector.getInstance(Config.class);
		config.load("config.properties");

	}

}
