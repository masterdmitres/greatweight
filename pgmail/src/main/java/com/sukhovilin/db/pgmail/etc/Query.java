package com.sukhovilin.db.pgmail.etc;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Query {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("query");
	private final Map<String, String> map = new ConcurrentHashMap<>();

	public void load(String fileName) throws IOException {

		String line = null;
		StringBuilder sb = null;
		String key = null;
		String splitter = "";

		log.debug("loading sql from file: {} ...", fileName);
		map.clear();

		try (FileInputStream in = new FileInputStream(fileName)) {
			try (InputStreamReader isr = new InputStreamReader(in)) {
				try (BufferedReader reader = new BufferedReader(isr)) {

					while ((line = reader.readLine()) != null) {
						line = line.trim();

						// skiping notes
						if (isnote(line)) continue;

						// process query
						if (ismarker(line)) {
							line = line.substring(1);
							if (key != null && sb != null) {
								map.put(key, sb.toString());
							}

							sb = new StringBuilder();
							key = line;
							splitter = "";
							log.debug("preparing query to load: {}", key);
							continue;
						}

						// processing sql text
						sb.append(splitter);
						sb.append(line);
						splitter = "\n";

					}

				} //

				map.put(key, sb.toString());
			}
		}
		
		log.info("total loaded: {}", map.size());

	}

	public String get(String key) throws IOException {
		String res = map.get(key);
		if (res == null) throw new IOException("no query: " + key);
		return res;
	}

	private boolean ismarker(String line) {
		if (line == null || line.isEmpty()) return false;
		return line.startsWith("?");
	}

	private boolean isnote(String line) {
		if (line == null || line.isEmpty()) return true;
		return line.startsWith("#");
	}

}
